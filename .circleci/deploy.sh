#!/usr/bin/env sh

# abort on errors
set -e

# build
npm run build

# navigate into the build output directory
cd dist

# git config
git config --global user.email "$GIT_AUTHOR_EMAIL"
git config --global user.name "$GIT_AUTHOR_NAME"

# if you are deploying to a custom domain
# echo 'www.example.com' > CNAME

mkdir .circleci
cat > .circleci/config.yml << EOF
version: 2
jobs:
  build:
    branches:
      ignore:
        - gh-pages
EOF

git init
git add -A
git commit -m 'deploy'

# if you are deploying to https://<USERNAME>.github.io
# git push -f git@github.com:<USERNAME>/<USERNAME>.github.io.git master

# if you are deploying to https://<USERNAME>.github.io/<REPO>
 git push -f git@github.com:arunscape/yt-downloader.git master:gh-pages

cd -
